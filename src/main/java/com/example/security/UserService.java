package com.example.security;

import com.example.security.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println(username);
        var user = userRepository.findByEmail(username).orElse(null);
        System.out.println(user+"::::::::::::::::"+user.getAuthorities());
        return User.withUsername(username).password(user.getPassword()).accountExpired(false)
                .accountLocked(false).credentialsExpired(false).authorities(user.getAuthorities()).build();
    }
}
